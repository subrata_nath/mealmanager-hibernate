package net.therap.mealmanager.Dao;

import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import net.therap.mealmanager.model.Type;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author subrata
 * @since 11/22/16
 */
public class MealDao extends GenericDao<Meal> {

    public MealDao() {
        classType = Meal.class;
    }

    public Set<Item> getItemsOfMeal(int id) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("id", id)).uniqueResult();

        Set<Item> items = new HashSet<>();

        for (Item item : meal.getItems()) {
            items.add(item);
        }

        transaction.commit();
        return items;
    }

    public List<Meal> getMealOfTheDay(Day day) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        List meals = session.createCriteria(Meal.class)
                .add(Restrictions.eq("day", day)).list();

        transaction.commit();
        return meals;
    }

    public void insertMeal(Day day, Type type, List<Item> newItems) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("type", type))
                .add(Restrictions.eq("day", day))
                .uniqueResult();

        Set<Item> items = new HashSet<>();

        if (meal == null) {
            meal = new Meal();
            meal.setDay(day);
            meal.setType(type);
        } else {
            items = meal.getItems();
        }

        for (Item item : newItems) {
            item = (Item) session.createCriteria(Item.class)
                    .add(Restrictions.eq("name", item.getName())).uniqueResult();
            if (item != null) {
                items.add(item);
            }
        }

        meal.setItems(items);
        session.persist(meal);
        transaction.commit();
    }

    public void deleteMeal(Day day, Type type, List<Item> itemsToBeDeleted) {

        Session session = getCurrentSession();
        Transaction transaction = session.beginTransaction();

        Meal meal = (Meal) session.createCriteria(Meal.class)
                .add(Restrictions.eq("type", type))
                .add(Restrictions.eq("day", day))
                .uniqueResult();

        Set<Item> items = new HashSet<>();

        if (meal != null) {
            for (Item item : meal.getItems()) {
                items.add(item);
            }
        }

        for (Item item : itemsToBeDeleted) {
            item = (Item) session.createCriteria(Item.class)
                    .add(Restrictions.eq("name", item.getName())).uniqueResult();
            items.remove(item);
        }

        meal.setItems(items);
        session.update(meal);
        transaction.commit();
    }
}
