package net.therap.mealmanager.controller;

import net.therap.mealmanager.service.FoodItemService;
import net.therap.mealmanager.utils.UI;

import java.util.Scanner;

/**
 * @author subrata
 * @since 11/13/16
 */
public class FoodItems {

    public FoodItems() {

        UI.showItems();

        while (true) {

            UI.showCommands(UI.FOOD_ITEMS_COMMANDS);
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();

            FoodItemService foodItemService = new FoodItemService();

            if (foodItemService.manipulateCommand(command) != true) {
                break;
            }
        }

    }

}
