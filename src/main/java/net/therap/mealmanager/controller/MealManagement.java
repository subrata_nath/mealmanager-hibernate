package net.therap.mealmanager.controller;

import net.therap.mealmanager.service.MealManagementService;
import net.therap.mealmanager.utils.UI;

import java.util.Scanner;

/**
 * @author subrata
 * @since 11/13/16
 */
public class MealManagement {

    public MealManagement() {

        UI.showDays();
        UI.showMealType();
        UI.showItems();

        while (true) {

            UI.showCommands(UI.MEAL_MANAGEMENT_COMMANDS);
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();

            MealManagementService mealManagementService = new MealManagementService();

            if (mealManagementService.manipulateCommand(command) != true) {
                break;
            }
        }
    }

}
