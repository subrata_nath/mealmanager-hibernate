package net.therap.mealmanager.controller;

import net.therap.mealmanager.utils.HibernateUtil;
import net.therap.mealmanager.utils.UI;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author subrata
 * @since 11/13/16
 */
public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        HibernateUtil.getSessionFactory();

        while (true) {

            UI.showWelcomeScreen();
            UI.showCommands(UI.MAIN_MENU_COMMANDS);

            try {

                String chooseCommand = scanner.nextLine();

                if (chooseCommand.equals("1") || chooseCommand.equals("one")) {

                    new FoodItems();

                } else if (chooseCommand.equals("2") || chooseCommand.equals("two")) {

                    new MealManagement();

                } else if (chooseCommand.equals("3") || chooseCommand.equals("three") || chooseCommand.equals("quit")) {

                    break;

                }

            } catch (Exception e) {

                UI.showErrorMessage();
            }
        }

    }
}