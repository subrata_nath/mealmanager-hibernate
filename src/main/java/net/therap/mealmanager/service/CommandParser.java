package net.therap.mealmanager.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author subrata
 * @since 11/22/16
 */
public class CommandParser {

    public static List<String> parseCommands(String command) {

        Pattern pattern = Pattern.compile("(\\w*[^ ,])");
        Matcher matcher = pattern.matcher(command);
        List<String> arguments = new ArrayList<>();

        while (matcher.find()) {
            String get = matcher.group();
            arguments.add(get);
        }

        return arguments;
    }
}
