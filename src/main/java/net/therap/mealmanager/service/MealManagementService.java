package net.therap.mealmanager.service;

import net.therap.mealmanager.Dao.MealDao;
import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import net.therap.mealmanager.model.Type;
import net.therap.mealmanager.utils.StringToEnum;
import net.therap.mealmanager.utils.UI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/15/16
 */
public class MealManagementService {

    private List<String> parameters;
    private Type type;
    private Day day;

    public boolean manipulateCommand(String command) {

        parameters = CommandParser.parseCommands(command);

        if (parameters.get(0).toLowerCase().equals("show")) {

            if(validateCommands(true,false)){

                MealDao mealDao = new MealDao();
                List<Meal> meals;
                meals = mealDao.getMealOfTheDay(day);

                for (Meal meal : meals) {

                    UI.showMeals(meal, mealDao.getItemsOfMeal(meal.getId()));
                }
            }

        } else if (parameters.get(0).toLowerCase().equals("ins")) {

            if(validateCommands(true,true)){
                List<Item> items = getItemsFromCommand();
                MealDao mealDao = new MealDao();
                mealDao.insertMeal(day, type, items);

            }

        } else if (parameters.get(0).toLowerCase().equals("del")) {

            if(validateCommands(true,true)){
                List<Item> items = getItemsFromCommand();
                MealDao mealDao = new MealDao();
                mealDao.deleteMeal(day, type, items);
            }

        } else if (parameters.get(0).toLowerCase().equals("quit")) {
            return false;
        } else {
            UI.showErrorMessage();
        }

        return true;
    }

    private List<Item> getItemsFromCommand() {

        List<Item> items = new ArrayList<>();

        for (int i = 3; i < parameters.size(); ++i) {
            Item item = new Item();
            item.setName(parameters.get(i));
            items.add(item);
        }
        return items;
    }

    private boolean validateCommands(boolean dayGiven, boolean typeGiven) {

        if (dayGiven && typeGiven) {

            day = StringToEnum.dayNameToDayEnum(parameters.get(1));
            type = StringToEnum.typeNameToTypeEnum(parameters.get(2));

            if (day == null || type == null) {
                UI.showNoResultsError();
                return false;
            }

        } else {

            day = StringToEnum.dayNameToDayEnum(parameters.get(1));

            if (day == null) {
                UI.showNoResultsError();

                return false;
            }
        }

        return true;
    }

}
