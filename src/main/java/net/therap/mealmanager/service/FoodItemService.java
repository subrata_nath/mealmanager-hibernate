package net.therap.mealmanager.service;

import net.therap.mealmanager.Dao.ItemDao;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.utils.UI;

import java.util.ArrayList;
import java.util.List;

/**
 * @author subrata
 * @since 11/15/16
 */
public class FoodItemService {

    private List<String> arguments;

    public boolean manipulateCommand(String command) {

        arguments = CommandParser.parseCommands(command);

        List<Item> items;
        ItemDao itemDao = new ItemDao();

        if (arguments.get(0).toLowerCase().equals("show")) {

            UI.showItems();

        } else if (arguments.get(0).toLowerCase().equals("ins")) {

            items = getItemsFromCommand();
            itemDao.addItems(items);
            UI.showItems();

        } else if (arguments.get(0).toLowerCase().equals("del")) {

            items = getItemsFromCommand();
            itemDao.deleteItemsByName(items);
            UI.showItems();

        } else if (arguments.get(0).toLowerCase().equals("quit")) {

            return false;

        } else {

            UI.showErrorMessage();

        }

        return true;
    }

    private List<Item> getItemsFromCommand() {

        List<Item> items = new ArrayList<>();

        for (int i = 1; i < arguments.size(); ++i) {

            Item item = new Item();
            item.setName(arguments.get(i));
            items.add(item);
        }

        return items;
    }
}
