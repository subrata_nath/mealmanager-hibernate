package net.therap.mealmanager.utils;

import net.therap.mealmanager.Dao.ItemDao;
import net.therap.mealmanager.model.Day;
import net.therap.mealmanager.model.Item;
import net.therap.mealmanager.model.Meal;
import net.therap.mealmanager.model.Type;

import java.util.List;
import java.util.Set;

/**
 * @author subrata
 * @since 11/14/16
 */
public class UI {

    public static final String UNDERLINE = "\n------------------------------------------------------------------\n";

    public static final String WELCOME_LABEL = String.format("%45s", "Welcome to Meal Management System");

    public static final String[] MAIN_MENU_COMMANDS =
            {"Press 1 : To edit available food items",
                    "Press 2 : To manage meal plan",
                    "Press 3 : Quit"};

    public static final String[] FOOD_ITEMS_COMMANDS =
            {"To show available food items: show",
                    "To insert new food items: ins [Food Name separated by comma]",
                    "To delete existing food items: del [Food Name separated by comma]",
                    "To quit: quit"};

    public static final String[] MEAL_MANAGEMENT_COMMANDS =
            {"To show meal plan of particular day : show [Day Name]",
                    "To insert items into meal plan : ins [Day Name] [Meal Type Name] [Food Name separated by comma]",
                    "To delete items from meal plan : del [Day Name] [Meal Type Name] [Food Name separated by comma]",
                    "To quit: quit"};

    public static final String ERROR = "----------------Bad Command----------------";

    public static final String NO_RESULTS_ERROR = "----------------No Results--------------";

    public static final String NO_ITEMS_ERROR = "No Available Food Items Yet";

    public static void showDays() {

        System.out.printf("%s%40s%s", UNDERLINE, "Available Days To Serve Meal", UNDERLINE);

        for (Day day : Day.values()) {
            System.out.printf("%20s: %s\n", day.ordinal() + 1, day);
        }

        System.out.printf("%s", UNDERLINE);
    }

    public static void showMealType() {

        System.out.printf("%s%40s%s", UNDERLINE, "Available Meal Types", UNDERLINE);

        for (Type type : Type.values()) {
            System.out.printf("%20s: %s\n", type.ordinal() + 1, type);
        }

        System.out.printf("%s", UNDERLINE);
    }

    public static void showItems() {

        ItemDao itemDao = new ItemDao();
        List<Item> items = itemDao.getItems();
        int itemCount = 1;

        System.out.printf("%s%40s%s", UNDERLINE, "Available Food Items", UNDERLINE);

        for (Item item : items) {

            System.out.printf("%20s: %s\n", itemCount, item.getName());
            ++itemCount;
        }

        System.out.printf("%s", UNDERLINE);

    }

    public static void showMeals(Meal meal, Set<Item> items) {

        System.out.printf("%s%50s%s  %10s  %10s  ", UNDERLINE, "Meal Plan for " + meal.getDay() + " " + meal.getType(),
                UNDERLINE, meal.getDay(), meal.getType());

        if (items.size() == 0) {
            System.out.printf("%40s\n%s", NO_ITEMS_ERROR, UNDERLINE);
            return;
        }

        String foodItems = "";

        for (Item item : items) {
            foodItems += item.getName() + ",";
        }

        System.out.printf("%40s\n%s", foodItems.substring(0, foodItems.length() - 1), UNDERLINE);
    }

    public static void showCommands(String[] commands) {

        System.out.println();

        for (String cmd : commands) {
            System.out.println(cmd);
        }

        System.out.print("\nYour Command: ");
    }

    public static void showWelcomeScreen() {

        System.out.println(UNDERLINE + WELCOME_LABEL + UNDERLINE);
    }

    public static void showErrorMessage() {
        System.out.println(ERROR);
    }

    public static void showNoResultsError() {
        System.out.println(NO_RESULTS_ERROR);
    }


}