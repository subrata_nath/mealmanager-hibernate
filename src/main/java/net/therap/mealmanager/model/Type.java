package net.therap.mealmanager.model;

/**
 * @author subrata
 * @since 11/22/16
 */
public enum Type {
    LUNCH, BREAKFAST
}
